package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;
import javax.swing.Timer;


/**
 * @author tb
 */
public abstract class Figura implements Runnable, ActionListener {

    protected boolean ans;
    // wspolny bufor
    protected Graphics2D buffer;
    protected Area area;
    // do wykreslania
    protected Shape shape;
    // przeksztalcenie obiektu
    protected AffineTransform aft;

    // przesuniecie
    private int dx, dy;
    // rozciaganie
    private double sf;
    // kat obrotu
    private double an;
    private int delay;

    private int width;
    private int height;

    private boolean tmpXB = false, tmpYB = false;
    private boolean tmpXS = false, tmpYS = false;

    protected Color clr;

    protected static final Random rand = new Random();

    public Figura(Graphics2D buf, int del, int w, int h) {
        delay = del;
        buffer = buf;
        width = w;
        height = h;

        dx = 1 + rand.nextInt(5);
        dy = 1 + rand.nextInt(5);
        sf = 1 + 0.05 * rand.nextDouble();
        an = 0.1 * rand.nextDouble();

        clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
    }

    @Override
    public void run() {
        // przesuniecie na srodek
        aft.translate(100, 100);
        area.transform(aft);
        shape = area;
        ans = true;

        while (true) {
            System.out.println();
            while (ans) {
                shape = nextFrame();
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    protected Shape nextFrame() {
        area = new Area(area);
        aft = new AffineTransform();

        Rectangle bounds = area.getBounds();

        int cx = bounds.x + bounds.width / 2;
        int cy = bounds.y + bounds.height / 2;


        if ((int) bounds.getMaxX() + 5 > width) {
            dx = -dx;
            if (tmpXB == true) {
                dx = -dx;
            }
            tmpXB = true;
        } else {
            tmpXB = false;
        }


        if ((int) bounds.getMinX() - 5 < 0) {
            dx = -dx;
            if (tmpXS == true) {
                dx = -dx;
            }
            tmpXS = true;
        } else {
            tmpXS = false;
        }



        if ((int) bounds.getMaxY() + 5> height) {
            dy = -dy;
            if (tmpYB == true) {
                dy = -dy;
            }
            tmpYB = true;
        } else {
            tmpYB = false;
        }



        if ((int) bounds.getMinY()  - 5 < 0) {
            dy = -dy;
            if (tmpYS == true) {
                dy = -dy;
            }
            tmpYS = true;
        } else {
            tmpYS = false;
        }


        // zwiekszenie lub zmniejszenie
        if (bounds.height > height / 3 || bounds.height < 10)
            sf = 1 / sf;

        // konstrukcja przeksztalcenia
        aft.translate(cx, cy);
        aft.scale(sf, sf);
        aft.rotate(an);
        aft.translate(-cx, -cy);
        aft.translate(dx, dy);

        // przeksztalcenie obiektu
        area.transform(aft);
        return area;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        // wypelnienie obiektu
        buffer.setColor(clr.brighter());
        buffer.fill(shape);
        // wykreslenie ramki
        buffer.setColor(clr.darker());
        buffer.draw(shape);
    }

}
